package com.develop_android;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    Button ex03_,ex04_,ex05_,ex06_,ex07_,ex08_,ex09_,ex10_;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ex03_ = findViewById(R.id.ex03);
        ex04_ = findViewById(R.id.ex04);
        ex05_ = findViewById(R.id.ex05);
        ex06_ = findViewById(R.id.ex06);
        ex07_ = findViewById(R.id.ex07);
        ex08_ = findViewById(R.id.ex08);
        ex09_ = findViewById(R.id.ex09);
        ex10_ = findViewById(R.id.ex10);
        ex03_.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent ex3 = new Intent(getApplicationContext(),ex03.class);
                startActivity(ex3);
            }
        });

        ex04_.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent ex4 = new Intent(getApplicationContext(),ex04.class);
                startActivity(ex4);
            }
        });
        ex05_.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent ex5 = new Intent(getApplicationContext(),ex05.class);
                startActivity(ex5);
            }
        });
        ex06_.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent ex6 = new Intent(getApplicationContext(),ex06.class);
                startActivity(ex6);
            }
        });
        ex07_.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent ex7 = new Intent(getApplicationContext(),ex07.class);
                startActivity(ex7);
            }
        });
        ex08_.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent ex8 = new Intent(getApplicationContext(),ex08.class);
                startActivity(ex8);
            }
        });
        ex09_.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent ex9 = new Intent(getApplicationContext(),ex09.class);
                startActivity(ex9);
            }
        });
        ex10_.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent ex010 = new Intent(getApplicationContext(),ex10.class);
                startActivity(ex010);
            }
        });
    }
}
