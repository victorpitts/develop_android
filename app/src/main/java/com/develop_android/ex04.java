package com.develop_android;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


public class ex04 extends AppCompatActivity {
    public TextView tituloex04_;
    Button op1;
    Button op2;
    Button op3;
    Button exec04;
    EditText hora_in_;
    EditText min_in_;
    EditText seg_in_;
    AlertDialog alerta;
    public double[] tarifa;
    private int hora,min,seg, op, tempo;
    private double preco;

    public void setHora_in(int h){
        this.hora = h;
    }

    public int getHora_in(){
        return this.hora;
    }
    public void setMin_in(int m){
        this.min = m;
    }
    public int getMin_in(){
        return this.min;
    }
    public void setSeg_in(int s){
        this.seg = s;
    }
    public int getSeg_in(){
        return this.seg;
    }
    public void setOp(int ope){
        this.op = ope;
    }
    public int getOp(){
        return this.op;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ex04);
        tituloex04_ = findViewById(R.id.tituloex04);
        op1 = findViewById(R.id.btn_op1);
        op2 = findViewById(R.id.btn_op2);
        op3 = findViewById(R.id.btn_op3);
        hora_in_ = findViewById(R.id.hora_in);
        min_in_ = findViewById(R.id.min_in);
        seg_in_ = findViewById(R.id.seg_in);
        tarifa = new double[3];
        tarifa[0] = 0.020;
        tarifa[1] = 0.025;
        tarifa[2] = 0.019;
        exec04 = findViewById(R.id.btn_exec04);
        op1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setOp(0);
            }
        });
        op2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setOp(1);
            }
        });
        op3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setOp(2);
            }
        });
        exec04.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hora = Integer.parseInt(String.valueOf(hora_in_.getText()));
                min = Integer.parseInt(String.valueOf(min_in_.getText()));
                seg = Integer.parseInt(String.valueOf(seg_in_.getText()));
                tempo  = 0;
                if(hora > 0){
                    tempo = hora * 3600;
                }
                if(min > 0){
                    tempo += min * 60;
                }
                if(seg > 0){
                    tempo += seg;
                }
                if(tempo >= 5){
                    tempo -= 5;
                }
                preco = tarifa[getOp()]*tempo;
                AlertDialog.Builder builder = new AlertDialog.Builder(ex04.this);
                builder.setTitle("Preço da Ligação");
                String oper = new String();
                if(getOp() == 0){
                    oper = "OP 01";
                }
                if(getOp() == 1){
                    oper = "OP 02";
                }
                if(getOp() == 2){
                    oper = "OP 03";
                }
                builder.setMessage(String.valueOf("Operadora = "+oper+" Tarifa/seg = "+tarifa[getOp()]+" Preço da Ligação = R$"+preco+""));
                alerta = builder.create();
                alerta.show();

            }
        });
    }
}
