package com.develop_android;

import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class ex03 extends AppCompatActivity {
    TextView tituloex03_;
    Button m_;
    Button f_;
    private String Sexo = "";
    Button btn_exec03_;
    EditText altura_in_;
    private float alt;



    private AlertDialog alerta;

    public void setSexo(String txt){
        this.Sexo = txt;
    }
    public String getSexo(){
        return this.Sexo;
    }

    public void setAlt(float altu){
        this.alt = altu;
    }
    public float getAlt(){
        return this.alt;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ex03);
        tituloex03_ = findViewById(R.id.tituloex03);
        m_ = findViewById(R.id.m);
        f_ = findViewById(R.id.f);
        altura_in_ = findViewById(R.id.altura_in);
        btn_exec03_ = findViewById(R.id.btn_exec03);

        m_.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               setSexo("M");
//               AlertDialog.Builder builder = new AlertDialog.Builder(ex03.this);
//               builder.setTitle("Titulo");
//               builder.setMessage(getSexo().toString());
//               alerta = builder.create();
//               alerta.show();
              // pesoideal_.setText(getSexo().toString());
           }
        });
        f_.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setSexo("F");
//                AlertDialog.Builder builder = new AlertDialog.Builder(ex03.this);
//                builder.setTitle("Titulo");
//                builder.setMessage(getSexo().toString());
//                alerta = builder.create();
//                alerta.show();
            }
        });

        btn_exec03_.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(getSexo().equals("")){
                    AlertDialog.Builder builder = new AlertDialog.Builder(ex03.this);
                    builder.setTitle("Erro");
                    builder.setMessage("Você precisa definir um Genero");
                    alerta = builder.create();
                    alerta.show();
                }
                else{
                    setAlt(Float.parseFloat(String.valueOf(altura_in_.getText())));
                    //pesoideal_.setText(String.valueOf(alt));
                    if(getSexo().equals("M")){
                        AlertDialog.Builder builder = new AlertDialog.Builder(ex03.this);
                        builder.setTitle("Peso Ideal");
                        double altf = Math.round((72.7 * getAlt())-58);
                        builder.setMessage(String.valueOf(getSexo()+" = "+altf+"Kg"));
                        alerta = builder.create();
                        alerta.show();

                        //tx1.setText(input01.getText().toString());
                    }
                    else{
                        AlertDialog.Builder builder = new AlertDialog.Builder(ex03.this);
                        builder.setTitle("Peso Ideal");
                        double altf = Math.round((62.1 * getAlt())-44.7);
                        builder.setMessage(String.valueOf(getSexo()+" = "+altf+"Kg"));
                        alerta = builder.create();
                        alerta.show();
                    }
                }

            }
        });

    }
}
